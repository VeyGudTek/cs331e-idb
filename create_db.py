import json
from datetime import datetime, timedelta
import os
import country_converter
from models import Game, Company, Platform, db, app

platform_categories = {
    1: "Console",
    2: "Arcade",
    3: "Platform",
    4: "Operating System",
    5: "Portable Console",
    6: "Computer"
}

country_codes = {}

def create_game(game):
    #SET COLUMNS IN TEMPORARY DICTIONARY TO ACCOUNT FOR MISSING COLUMNS IN JSON
    temp_game = {"id": None, "name": None, "cover": None, "rating": None, "involved_companies": [], 'platforms': []}
    for col in temp_game:
        if col in game:
            temp_game[col] = game[col]

    #CONVERT COVER DICT TO URL
    if temp_game['cover']:
        temp_game['cover'] = temp_game['cover']['url']

    new_game = Game(id=temp_game['id'], name=temp_game['name'], cover=temp_game['cover'], rating=temp_game['rating'])
    db.session.add(new_game)
    db.session.commit()

    #ADD INVOLVED COMPANIES
    for involved_company in temp_game['involved_companies']:
        #GET EXISTING COMPANY, OR CREATE NEW
        if db.session.query(Company).filter(Company.id == involved_company['company']['id']).all():
            new_company = db.session.query(Company).filter(Company.id == involved_company['company']['id']).first()
        else:
            new_company = create_company(involved_company['company'])

        #APPEND COMPANY TO MANY TO MANY
        if involved_company['developer']:
            new_game.developers.append(new_company)
        if involved_company['publisher']:
            new_game.publishers.append(new_company)

    #ADD PLATFORMS
    for platform in temp_game['platforms']:
        #GET EXISTING OR CREATE NEW
        if db.session.query(Platform).filter(Platform.id == platform['id']).all():
            new_platform = db.session.query(Platform).filter(Platform.id == platform['id']).first()
        else:
            new_platform = create_platform(platform)

        new_game.platforms.append(new_platform)
    db.session.commit()
            

def create_company(company):
    #SET COLUMNS IN TEMPORARY DICTIONARY TO ACCOUNT FOR MISSING COLUMNS IN JSON
    temp_company = {'id': None, "name": None, "country": None, "description": None, "start_date": None, "logo": None}
    for col in temp_company:
        if col in company:
            temp_company[col] = company[col]

    #CONVERT LOGO DICT TO URL, ADD PLACEHOLDER IF NONE
    if temp_company['logo']:
        temp_company['logo'] = temp_company['logo']['url']
    else:
        temp_company['logo'] = "../static/images/no-photos.png"
    #CONVERT UNIX TO DATE
    if temp_company['start_date']:
        if temp_company['start_date'] < 0 and len(str(abs(temp_company['start_date']))) >= 11:
            temp_company['start_date'] = datetime(1970, 1, 1) + timedelta(milliseconds=temp_company['start_date'])
        elif temp_company['start_date'] < 0:
            temp_company['start_date'] = datetime(1970, 1, 1) + timedelta(seconds=temp_company['start_date'])
        else:
            temp_company['start_date'] = datetime.utcfromtimestamp(temp_company['start_date'])
            #temp_company['start_date'] = datetime(1970, 1, 1) + timedelta(microseconds=temp_company['start_date'])
    #CONVERT ISO 3166-1 COUNTRY CODE TO NAME
    if temp_company['country']:
        if not temp_company['country'] in country_codes:
            country_codes[temp_company['country']] = country_converter.convert(names=[temp_company['country']], to='name')
            
        temp_company['country'] = country_codes[temp_company['country']]

    new_company = Company(id=temp_company['id'], name=temp_company['name'], country=temp_company['country'], description=temp_company['description'], start_date=temp_company['start_date'], logo=temp_company['logo'])
    db.session.add(new_company)
    db.session.commit()

    return new_company


def create_platform(platform):
    #SET COLUMNS IN TEMPORARY DICTIONARY TO ACCOUNT FOR MISSING COLUMNS IN JSON
    temp_platform = {'id': None, 'name': None, 'platform_logo': None, 'category': None, 'websites': None}
    for col in temp_platform:
        if col in platform:
            temp_platform[col] = platform[col]

    #CONVERT LOGO AND WEBSITE LIST TO URL, CATEGORY ENUM TO STRING
    if temp_platform['platform_logo']:
        temp_platform['platform_logo'] = temp_platform['platform_logo']['url']
    else:
        temp_platform['platform_logo'] = "../static/images/no-photos.png"
    if temp_platform['websites']:
        temp_platform['websites'] = temp_platform['websites'][0]['url']
    if temp_platform['category']:
        temp_platform['category'] = platform_categories[temp_platform['category']]

    new_platform = Platform(id=temp_platform['id'], name=temp_platform['name'], logo=temp_platform['platform_logo'], category=temp_platform['category'], website=temp_platform["websites"])
    db.session.add(new_platform)
    db.session.commit()

    return new_platform

with open('games.json', 'r') as games_file:
    games = json.load(games_file)

    for game in games:
        create_game(game)