import unittest
from datetime import datetime
from models import db, Game, Company, Platform

# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):
    #CREATE ENTRIES BEFORE EACH TEST
    def setUp(self):
        #Create Game
        game = Game(id=16, name='Bob the Builder: The Game', cover="../static/images/no-photos.png", rating=66.43)
        db.session.add(game)
        db.session.commit()

        #Create Company
        company_1 = Company(id=20, name='Cover Corporation', logo="../static/images/no-photos.png", description='This company is really based.', country='Yemen', start_date=datetime(1969, 4, 20))
        db.session.add(company_1)
        company_2 = Company(id=13, name='Phase Connect', logo="../static/images/no-photos.png", description='No one has heard of this company', country='Yugoslavia', start_date=datetime(1953, 6, 13))
        db.session.add(company_2)
        db.session.commit()

        #Create Platform
        platform = Platform(id=25, name='TempleOS', logo="../static/images/no-photos.png", category="Operating System", website="https://templeos.org/")
        db.session.add(platform)
        db.session.commit()

        #Connect Models
        game.developers.append(company_1)
        game.publishers.append(company_2)
        game.platforms.append(platform)
        db.session.commit()

    #REMOVE ENTRIES AFTER EACH TEST
    def tearDown(self):
        db.session.query(Company).filter_by(id = 20).delete()
        db.session.query(Company).filter_by(id = 13).delete()
        db.session.query(Platform).filter_by(id = 25).delete()
        db.session.query(Game).filter_by(id = 16).delete()
        db.session.commit()

    #GAME TESTS
    def test_game_insert_1(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.id, 16)
    
    def test_game_insert_2(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.name, 'Bob the Builder: The Game')

    def test_game_insert_3(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.rating, 66.43)

    def test_game_connection_1(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.publishers[0].name, "Phase Connect")
    
    def test_game_connection_2(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.publishers[0].description, "No one has heard of this company")

    def test_game_connection_3(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.publishers[0].country, "Yugoslavia")

    def test_game_connection_4(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.developers[0].name, "Cover Corporation")

    def test_game_connection_5(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.developers[0].country, "Yemen")

    def test_game_connection_6(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.developers[0].description, "This company is really based.")

    def test_game_connection_7(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.platforms[0].name, "TempleOS")

    def test_game_connection_8(self):
        r = db.session.query(Game).filter_by(id = 16).one()
        self.assertEqual(r.platforms[0].category, "Operating System")

    #COMPANY TESTS
    def test_company_insert_1(self):
        r = db.session.query(Company).filter_by(id = 20).one()
        self.assertEqual(r.id, 20)

    def test_company_insert_2(self):
        r = db.session.query(Company).filter_by(id = 20).one()
        self.assertEqual(r.name, "Cover Corporation")

    def test_company_insert_3(self):
        r = db.session.query(Company).filter_by(id = 20).one()
        self.assertEqual(r.country, "Yemen")

    def test_company_insert_4(self):
        r = db.session.query(Company).filter_by(id = 13).one()
        self.assertEqual(r.id, 13)

    def test_company_insert_5(self):
        r = db.session.query(Company).filter_by(id = 13).one()
        self.assertEqual(r.name, "Phase Connect")

    def test_company_insert_6(self):
        r = db.session.query(Company).filter_by(id = 13).one()
        self.assertEqual(r.country, "Yugoslavia")

    def test_company_connection_1(self):
        r = db.session.query(Company).filter_by(id = 13).one()
        self.assertEqual(r.published[0].name, "Bob the Builder: The Game")

    def test_company_connection_2(self):
        r = db.session.query(Company).filter_by(id = 13).one()
        self.assertEqual(r.published[0].rating, 66.43)

    def test_company_connection_3(self):
        r = db.session.query(Company).filter_by(id = 20).one()
        self.assertEqual(r.developed[0].name, "Bob the Builder: The Game")

    def test_company_connection_4(self):
        r = db.session.query(Company).filter_by(id = 20).one()
        self.assertEqual(r.developed[0].rating, 66.43)

    #PLATFORM TESTS
    def test_platform_insert_1(self):
        r = db.session.query(Platform).filter_by(id = 25).one()
        self.assertEqual(r.id, 25)

    def test_platform_insert_2(self):
        r = db.session.query(Platform).filter_by(id = 25).one()
        self.assertEqual(r.name, "TempleOS")

    def test_platform_insert_3(self):
        r = db.session.query(Platform).filter_by(id = 25).one()
        self.assertEqual(r.category, "Operating System")

    def test_platform_connection_1(self):
        r = db.session.query(Platform).filter_by(id = 25).one()
        self.assertEqual(r.games[0].name, "Bob the Builder: The Game")

    def test_platform_connection_2(self):
        r = db.session.query(Platform).filter_by(id = 25).one()
        self.assertEqual(r.games[0].id, 16)


if __name__ == '__main__':
    unittest.main()
