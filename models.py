from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)

app.app_context().push()

#USER DATABASE INFO
USER = 'postgres'

# Local Setup
# PASSWORD = '1234'
# PUBLIC_IP_ADDRESS = 'localhost:5432'

# GCP Setup
PASSWORD = 'Gumb0Fumb0'
PUBLIC_IP_ADDRESS = '34.133.164.166'

DBNAME = "classtest"

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", f"postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

#MANY TO MANY RELATIONSHIPS
developer_link = db.Table('developer_link',
   db.Column('game_id', db.Integer, db.ForeignKey('game.id', ondelete="CASCADE")), 
   db.Column('company_id', db.Integer, db.ForeignKey('company.id', ondelete="CASCADE"))
   )

publisher_link = db.Table('publisher_link',
   db.Column('game_id', db.Integer, db.ForeignKey('game.id', ondelete="CASCADE")), 
   db.Column('company_id', db.Integer, db.ForeignKey('company.id', ondelete="CASCADE"))
   )

platform_link = db.Table('platform_link',
   db.Column('game_id', db.Integer, db.ForeignKey('game.id', ondelete="CASCADE")), 
   db.Column('platform_id', db.Integer, db.ForeignKey('platform.id', ondelete="CASCADE"))
   )

#MODELS
class Game(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    cover = db.Column(db.String, nullable = True)
    rating = db.Column(db.Float, nullable = True)
    developers = db.relationship('Company', secondary=developer_link, backref="developed")
    publishers = db.relationship('Company', secondary=publisher_link, backref="published")
    platforms = db.relationship('Platform', secondary=platform_link, backref="games")

class Company(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    country = db.Column(db.String)
    description = db.Column(db.String, nullable=True)
    start_date = db.Column(db.Date)
    logo = db.Column(db.String, nullable=True)

class Platform(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    logo = db.Column(db.String, nullable = True)
    category = db.Column(db.String)
    website = db.Column(db.String, nullable = True)


db.drop_all()
db.create_all()