from flask import Flask, render_template, request
from create_db import app, db, Game, Company, Platform
import math, json

temp_games = [	{'id': 4, 'name': 'Thief', 'rating': 70.26, "cover":"//images.igdb.com/igdb/image/upload/t_720p/co22nc.png", "developers": [{'id': 27, 'name': 'Eidos Montr�al'}], "publishers": [{'id': 26, 'name': 'Square Enix'}], 'platforms': [{'id': 6, 'name': 'PC (Microsoft Windows)'}, {'id': 14, 'name': 'Mac'}]},\
				{'id': 5, 'name': "Baldur's Gate", "rating": 86.00, "cover":"//images.igdb.com/igdb/image/upload/t_720p/co20gb.png", "developers": [{'id': 2, 'name': 'BioWare'}, {'id': 222, 'name': 'BioWare Edmonton'}], "publishers": [{'id': 12, 'name': 'Black Isle Studios'}, {'id': 5, 'name': 'Interplay Entertainment'}], 'platforms': [{'id': 6, 'name': 'PC (Microsoft Windows)'}, {'id': 14, 'name': 'Mac'}, {'id': 3, 'name': 'Linux'}]},\
				{'id': 6, 'name': "Baldur's Gate II: Shadows of Amn", "rating": 87.76, "cover":"//images.igdb.com/igdb/image/upload/t_720p/co71yr.png", "developers": [{'id': 2, 'name': 'BioWare'}, {'id': 222, 'name': 'BioWare Edmonton'}], "publishers": [{'id': 12, 'name': 'Black Isle Studios'}, {'id': 5, 'name': 'Interplay Entertainment'}], 'platforms': [{'id': 6, 'name': 'PC (Microsoft Windows)'}, {'id': 14, 'name': 'Mac'}, {'id': 3, 'name': 'Linux'}] }]

temp_companies = [ 	{'id': 2, 'name': "Bioware", "start_date": 791596800, "country": 124, "logo": "//images.igdb.com/igdb/image/upload/t_720p/cl4vp.png", "developed":[{'id': 5, 'name': "Baldur's Gate"}, {'id': 6, 'name': "Baldur's Gate II: Shadows of Amn"}], "published":[{'id': 13784, 'name': 'Mass Effect: Bring Down the Sky'}, {'id': 22963, 'name': 'Dragon Age: Origins - Darkspawn Chronicles'}], "description": "BioWare is a Canadian video game developer founded by newly graduated medical doctors Ray Muzyka, Greg Zeschuk and Augustine Yip, alongside Trent Oster, Brent Oster, and Marcel Zeschuk. The studio specializes in role-playing video games and achieved recognition for developing highly praised and successful licensed franchises."},\
					{'id': 12, 'name': "Black Isle Studios", "start_date": 828230400, "country": "n/a", "logo": "//images.igdb.com/igdb/image/upload/t_720p/veezgocmhiauoqbkfita.png",  "developed":[{'id': 13, 'name': 'Fallout'}, {'id': 14, 'name': 'Fallout 2'}], "published":[{'id': 5, 'name': "Baldur's Gate"}, {'id': 6, 'name': "Baldur's Gate II: Shadows of Amn"}], "description": "n/a"},\
					{'id': 26, 'name': "Square Enix", "start_date": 'n/a', "country": 392, "logo": "//images.igdb.com/igdb/image/upload/t_720p/yqsot4fytthxblnyvcia.png", "developed":[{'id': 359, 'name': 'Final Fantasy XV'}, {'id': 383, 'name': 'Final Fantasy Type-0'}], "published":[{'id': 4, 'name': 'Thief'},{'id': 43, 'name': 'Deus Ex: Human Revolution'}], "description": "Square Enix develops, publishes, distributes and licenses Square Enix, Eidos and Taito branded entertainment content in Europe and other PAL territories as part of the Square Enix group of companies. Square Enix also has a global network of leading development studios such as Crystal Dynamics and Eidos Montr�al. The Square Enix group of companies boasts a valuable portfolio of intellectual property including: Final Fantasy, which has sold over 130 million units worldwide; Dragon Quest, which has sold over 71 million units worldwide; Tomb Raider, which has sold over 58 million units worldwide; and the legendary Space Invaders."}]

temp_platforms = [ 	{'id': 6, 'name': 'PC (Microsoft Windows)', 'category': "operating_system", "website": "http://windows.microsoft.com/",'logo': '//images.igdb.com/igdb/image/upload/t_720p/irwvwpl023f8y19tidgq.png', 'games': [{'id': 4, 'name': 'Thief'}, {'id': 5, 'name': "Baldur's Gate"}, {'id': 6, 'name': "Baldur's Gate II: Shadows of Amn"}]},\
					{'id': 14, 'name': 'Mac', 'category': "operating_system", "website": "https://www.apple.com/osx/", 'logo': "https://i.etsystatic.com/24423171/r/il/3f60f3/5599338649/il_fullxfull.5599338649_rcwe.jpg", 'games': [{'id': 4, 'name': 'Thief'}, {'id': 5, 'name': "Baldur's Gate"}, {'id': 6, 'name': "Baldur's Gate II: Shadows of Amn"}]},\
					{'id': 3, 'name': 'Linux', 'category': "operating_system", "website": "http://www.linux.org", 'logo': '//images.igdb.com/igdb/image/upload/t_720p/plak.png', 'games': [{'id': 5, 'name': "Baldur's Gate"}, {'id': 6, 'name': "Baldur's Gate II: Shadows of Amn"}]}]

# note that Mac does not have a logo, it was manually added in for display purposes

#Pagination
CARDS_PER_PAGE = 8
def get_pages(total_cards):
	last_page = math.ceil(total_cards / CARDS_PER_PAGE)

	page = request.args.get('page')
	if not page or (not page.isdigit()) or int(page) < 1:
		page = 1
	elif int(page) > last_page:
		page = last_page
	else:
		page = int(page)
	prev_page = page - 1
	next_page = page + 1

	return page, prev_page, next_page, last_page


@app.route("/")
@app.route("/home")
def home():
    return render_template('index.html', current_path=request.path)

@app.route("/games")
def games_list():
	games_list = db.session.query(Game)

	#Search
	search = request.args.get('search')
	if search:
		games_list = Game.query.filter(Game.name.ilike('%' + search + '%'))

	#Sort and Order
	sort_by = request.args.get('sort')
	order_by = request.args.get('order')
	if sort_by == 'rating':
		if order_by == 'desc':
			games_list = games_list.order_by(Game.rating.desc())
		else:
			games_list = games_list.order_by(Game.rating.asc())
	else:
		if order_by == 'desc':
			games_list = games_list.order_by(Game.name.desc())
		else:
			games_list = games_list.order_by(Game.name.asc())

	#Pagination
	page, prev_page, next_page, last_page = get_pages(games_list.count())
	games_list = games_list.limit(CARDS_PER_PAGE).offset(CARDS_PER_PAGE * (page - 1)).all()
	return render_template("games_list.html", games_list = games_list, page=page, prev_page=prev_page, next_page=next_page, last_page=last_page, sort=sort_by, order=order_by, search=search)

@app.route('/api/games')
def games_list_api():
	games_list = db.session.query(Game)

	#Search
	search = request.args.get('search')
	if search:
		games_list = Game.query.filter(Game.name.ilike('%' + search + '%'))

	#Sort and Order
	sort_by = request.args.get('sort')
	order_by = request.args.get('order')
	if sort_by == 'name':
		if order_by == 'desc':
			games_list = games_list.order_by(Game.name.desc())
		else:
			games_list = games_list.order_by(Game.name.asc())
	elif sort_by == 'rating':
		if order_by == 'desc':
			games_list = games_list.order_by(Game.rating.desc())
		else:
			games_list = games_list.order_by(Game.rating.asc())
	
	#Limit and Offset
	limit = request.args.get('limit')
	offset = request.args.get('offset')
	if limit and limit.isdigit() and int(limit) >= 1:
		games_list = games_list.limit(int(limit))
	if offset and offset.isdigit() and int(offset) >= 1:
		games_list = games_list.offset(int(offset))

	#Convert to Query to List of Dictionaries
	games_list = games_list.all()
	dict_list = []
	for game in games_list:
		dict_list.append({col.name: getattr(game, col.name) for col in game.__table__.columns})
	return json.dumps(dict_list)

@app.route("/games/<int:game_id>")
def game_page(game_id):
	game = db.session.query(Game).filter(Game.id == game_id).first()

	return render_template("game_page.html", game = game)

@app.route('/api/games/<int:game_id>')
def game_page_api(game_id):
	game = db.session.query(Game).filter(Game.id == game_id).first()
	result = {}
	if game:
		result = {col.name: getattr(game, col.name) for col in game.__table__.columns}
	return json.dumps(result)

@app.route("/companies")
def companies_list():
	companies_list = db.session.query(Company)

	#Search
	search = request.args.get('search')
	if search:
		companies_list = Company.query.filter(Company.name.ilike("%" + search + "%"))

	#Sort and Order
	sort_by = request.args.get('sort')
	order_by = request.args.get('order')
	if sort_by == 'country':
		if order_by == 'desc':
			companies_list = companies_list.order_by(Company.country.desc())
		else:
			companies_list = companies_list.order_by(Company.country.asc())
	elif sort_by == 'date':
		if order_by == 'desc':
			companies_list = companies_list.order_by(Company.start_date.desc())
		else:
			companies_list = companies_list.order_by(Company.start_date.asc())
	else:
		if order_by == 'desc':
			companies_list = companies_list.order_by(Company.name.desc())
		else:
			companies_list = companies_list.order_by(Company.name.asc())

	#Remove Companies with empty description
	companies_list = companies_list.filter(Company.description != None)

	#Pagination
	page, prev_page, next_page, last_page = get_pages(companies_list.count())
	companies_list = companies_list.limit(CARDS_PER_PAGE).offset(CARDS_PER_PAGE * (page - 1)).all()
	return render_template("companies_list.html", companies_list = companies_list, page=page, prev_page=prev_page, next_page=next_page, last_page=last_page, sort=sort_by, order=order_by, search=search)

@app.route("/api/companies")
def companies_list_api():
	companies_list = db.session.query(Company)

	#Search
	search = request.args.get('search')
	if search:
		companies_list = Company.query.filter(Company.name.ilike("%" + search + "%"))

	#Sort and Order
	sort_by = request.args.get('sort')
	order_by = request.args.get('order')
	if sort_by == 'name':
		if order_by == 'desc':
			companies_list = companies_list.order_by(Company.name.desc())
		else:
			companies_list = companies_list.order_by(Company.name.asc())
	elif sort_by == 'country':
		if order_by == 'desc':
			companies_list = companies_list.order_by(Company.country.desc())
		else:
			companies_list = companies_list.order_by(Company.country.asc())
	elif sort_by == 'date':
		if order_by == 'desc':
			companies_list = companies_list.order_by(Company.start_date.desc())
		else:
			companies_list = companies_list.order_by(Company.start_date.asc())

	#Limit and Offset
	limit = request.args.get('limit')
	offset = request.args.get('offset')
	if limit and limit.isdigit() and int(limit) >= 1:
		companies_list = companies_list.limit(int(limit))
	if offset and offset.isdigit() and int(offset) >= 1:
		companies_list = companies_list.offset(int(offset))

	#Convert Query to List of Dictionaries
	companies_list = companies_list.all()
	dict_list = []
	for company in companies_list:
		dict_list.append({col.name: str(getattr(company, col.name)) for col in company.__table__.columns})
	return json.dumps(dict_list)

@app.route("/companies/<int:company_id>")
def company_page(company_id):
	company = db.session.query(Company).filter(Company.id == company_id).first()

	return render_template("company_page.html", company = company)

@app.route("/api/companies/<int:company_id>")
def company_page_api(company_id):
	company = db.session.query(Company).filter(Company.id == company_id).first()
	result = {}
	if company:
		result = {col.name: str(getattr(company, col.name)) for col in company.__table__.columns}
	return json.dumps(result)

@app.route("/platforms")
def platforms_list():
	platforms_list = db.session.query(Platform)

	#Search
	search = request.args.get('search')
	if search:
		platforms_list = Platform.query.filter(Platform.name.ilike("%" + search + "%"))

	#Sort and Order
	sort_by = request.args.get('sort')
	order_by = request.args.get('order')
	if sort_by == "category":
		if order_by == 'desc':
			platforms_list = platforms_list.order_by(Platform.category.desc())
		else:
			platforms_list = platforms_list.order_by(Platform.category.asc())
	else:
		if order_by == 'desc':
			platforms_list = platforms_list.order_by(Platform.name.desc())
		else:
			platforms_list = platforms_list.order_by(Platform.name.asc())

	#Pagination
	page, prev_page, next_page, last_page = get_pages(platforms_list.count())
	platforms_list = platforms_list.limit(CARDS_PER_PAGE).offset(CARDS_PER_PAGE * (page - 1)).all()
	return render_template("platforms_list.html", platforms_list = platforms_list, page=page, prev_page=prev_page, next_page=next_page, last_page=last_page, sort=sort_by, order=order_by, search=search)

@app.route("/api/platforms")
def platforms_list_api():
	platforms_list = db.session.query(Platform)

	#Search
	search = request.args.get('search')
	if search:
		platforms_list = Platform.query.filter(Platform.name.ilike("%" + search + "%"))

	#Sort and Order
	sort_by = request.args.get('sort')
	order_by = request.args.get('order')
	if sort_by == "name":
		if order_by == 'desc':
			platforms_list = platforms_list.order_by(Platform.name.desc())
		else:
			platforms_list = platforms_list.order_by(Platform.name.asc())
	elif sort_by == "category":
		if order_by == 'desc':
			platforms_list = platforms_list.order_by(Platform.category.desc())
		else:
			platforms_list = platforms_list.order_by(Platform.category.asc())

	#Limit and Offset
	limit = request.args.get('limit')
	offset = request.args.get('offset')
	if limit and limit.isdigit() and int(limit) >= 1:
		platforms_list = platforms_list.limit(int(limit))
	if offset and offset.isdigit() and int(offset) >= 1:
		platforms_list = platforms_list.offset(int(offset))
	
	#Convert Query to List of Dictionaries
	platforms_list = platforms_list.all()
	dict_list = []
	for platform in platforms_list:
		dict_list.append({col.name: getattr(platform, col.name) for col in platform.__table__.columns})
	return json.dumps(dict_list)

@app.route("/platforms/<int:platform_id>")
def platform_page(platform_id):
	platform = db.session.query(Platform).filter(Platform.id == platform_id).first()
			
	return render_template("platform_page.html", platform = platform)

@app.route('/api/platforms/<int:platform_id>')
def platform_page_api(platform_id):
	platform = db.session.query(Platform).filter(Platform.id == platform_id).first()
	result = {}
	if platform:
		result = {col.name: getattr(platform, col.name) for col in platform.__table__.columns}
	return json.dumps(result)

@app.route("/about")
def about():
	return render_template("about.html")

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5001)